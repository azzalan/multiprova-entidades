'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Questao = undefined;

var _Entidade2 = require('../Entidade');

var _questaoProps = require('./questaoProps');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Questao = exports.Questao = function (_Entidade) {
  _inherits(Questao, _Entidade);

  function Questao(data, operationType) {
    _classCallCheck(this, Questao);

    var _this = _possibleConstructorReturn(this, (Questao.__proto__ || Object.getPrototypeOf(Questao)).call(this, operationType));

    _this.construirFront(_questaoProps.questaoProps, data);
    return _this;
  }

  return Questao;
}(_Entidade2.Entidade);