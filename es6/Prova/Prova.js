import { Entidade } from '../Entidade'
import { provaProps } from './provaProps'

export class Prova extends Entidade {
  constructor(data, operationType) {
    super(operationType)
    this.construirFront(provaProps, data)
  }
}
