'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Usuario = require('./Usuario');

Object.defineProperty(exports, 'Usuario', {
  enumerable: true,
  get: function get() {
    return _Usuario.Usuario;
  }
});

var _usuarioProps = require('./usuarioProps');

Object.defineProperty(exports, 'usuarioProps', {
  enumerable: true,
  get: function get() {
    return _usuarioProps.usuarioProps;
  }
});