import { Entidade } from '../Entidade'
import { usuarioProps } from './usuarioProps'

const ADMIN = 1
const DOCENTE = 2
const DISCENTE = 3

export class Usuario extends Entidade {
  constructor(data, operationType) {
    super(operationType)
    this.construirFront(usuarioProps, data)
  }

  isAdmin() {
    this.throwIfInvalid()
    return this.data.permissoes.includes(ADMIN)
  }

  setarComoAdmin() {
    if (!this.isAdmin()) this.data.permissoes.push(ADMIN)
    else console.error('Tentando setar como admin usuário que já é admin.')
  }

  isDocente() {
    this.throwIfInvalid()
    return this.data.permissoes.includes(DOCENTE)
  }

  setarComoDocente() {
    if (!this.isDocente()) this.data.permissoes.push(DOCENTE)
    else console.error('Tentando setar como docente usuário que já é docente.')
  }

  isDiscente() {
    this.throwIfInvalid()
    return this.data.permissoes.includes(DISCENTE)
  }

  setarComoDiscente() {
    if (!this.isDiscente()) this.data.permissoes.push(DISCENTE)
    else console.error('Tentando setar como discente usuário que já é discente.')
  }
}
