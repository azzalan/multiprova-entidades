'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Usuario = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entidade2 = require('../Entidade');

var _usuarioProps = require('./usuarioProps');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ADMIN = 1;
var DOCENTE = 2;
var DISCENTE = 3;

var Usuario = exports.Usuario = function (_Entidade) {
  _inherits(Usuario, _Entidade);

  function Usuario(data, operationType) {
    _classCallCheck(this, Usuario);

    var _this = _possibleConstructorReturn(this, (Usuario.__proto__ || Object.getPrototypeOf(Usuario)).call(this, operationType));

    _this.construirFront(_usuarioProps.usuarioProps, data);
    return _this;
  }

  _createClass(Usuario, [{
    key: 'isAdmin',
    value: function isAdmin() {
      this.throwIfInvalid();
      return this.data.permissoes.includes(ADMIN);
    }
  }, {
    key: 'setarComoAdmin',
    value: function setarComoAdmin() {
      if (!this.isAdmin()) this.data.permissoes.push(ADMIN);else console.error('Tentando setar como admin usuário que já é admin.');
    }
  }, {
    key: 'isDocente',
    value: function isDocente() {
      this.throwIfInvalid();
      return this.data.permissoes.includes(DOCENTE);
    }
  }, {
    key: 'setarComoDocente',
    value: function setarComoDocente() {
      if (!this.isDocente()) this.data.permissoes.push(DOCENTE);else console.error('Tentando setar como docente usuário que já é docente.');
    }
  }, {
    key: 'isDiscente',
    value: function isDiscente() {
      this.throwIfInvalid();
      return this.data.permissoes.includes(DISCENTE);
    }
  }, {
    key: 'setarComoDiscente',
    value: function setarComoDiscente() {
      if (!this.isDiscente()) this.data.permissoes.push(DISCENTE);else console.error('Tentando setar como discente usuário que já é discente.');
    }
  }]);

  return Usuario;
}(_Entidade2.Entidade);