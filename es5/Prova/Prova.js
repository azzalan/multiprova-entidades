'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Prova = undefined;

var _Entidade2 = require('../Entidade');

var _provaProps = require('./provaProps');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Prova = exports.Prova = function (_Entidade) {
  _inherits(Prova, _Entidade);

  function Prova(data, operationType) {
    _classCallCheck(this, Prova);

    var _this = _possibleConstructorReturn(this, (Prova.__proto__ || Object.getPrototypeOf(Prova)).call(this, operationType));

    _this.construirFront(_provaProps.provaProps, data);
    return _this;
  }

  return Prova;
}(_Entidade2.Entidade);