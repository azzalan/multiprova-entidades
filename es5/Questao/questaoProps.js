'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var questaoProps = exports.questaoProps = {
  id: {
    type: 'string',
    auto: {
      create: true
    }
  },
  dataCadastro: {
    type: 'date',
    auto: {
      create: true
    }
  },
  dataUltimaAlteracao: {
    type: 'date',
    auto: {
      create: true,
      update: true
    }
  },
  criadoPor: {
    type: 'string',
    auto: {
      create: true
    }
  },
  usuarioId: {
    type: 'string',
    auto: {
      create: true
    }
  },
  enunciado: {
    type: 'string',
    required: true
  },
  dificuldade: {
    type: 'number',
    required: true
  },
  tipo: {
    type: 'string',
    required: true
  },
  multiplaEscolha: {
    type: 'object',
    required: true
  }
};