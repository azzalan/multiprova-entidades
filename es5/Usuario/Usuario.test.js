'use strict';

var _Usuario = require('./Usuario');

describe('Usuario', function () {
  it('cria', function () {
    var data = {
      nome: 'ODocente SobrenomeDocente',
      instituicao: 'UFRN',
      permissoes: [2],
      username: 'docente',
      email: 'docente@ufrn.br',
      password: '12345678'
    };
    var usuario = new _Usuario.Usuario(data, 'create');
    expect(usuario.isValid()).toBeTruthy();
    expect(usuario.data.nome).toBe(data.nome);
    expect(usuario.data.instituicao).toBe(data.instituicao);
    expect(usuario.data.permissoes).toBe(data.permissoes);
    expect(usuario.data.username).toBe(data.username);
    expect(usuario.data.email).toBe(data.email);
  });
});