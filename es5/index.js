'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Usuario = require('./Usuario');

Object.defineProperty(exports, 'Usuario', {
  enumerable: true,
  get: function get() {
    return _Usuario.Usuario;
  }
});
Object.defineProperty(exports, 'usuarioProps', {
  enumerable: true,
  get: function get() {
    return _Usuario.usuarioProps;
  }
});

var _Questao = require('./Questao');

Object.defineProperty(exports, 'Questao', {
  enumerable: true,
  get: function get() {
    return _Questao.Questao;
  }
});
Object.defineProperty(exports, 'questaoProps', {
  enumerable: true,
  get: function get() {
    return _Questao.questaoProps;
  }
});

var _Prova = require('./Prova');

Object.defineProperty(exports, 'Prova', {
  enumerable: true,
  get: function get() {
    return _Prova.Prova;
  }
});
Object.defineProperty(exports, 'provaProps', {
  enumerable: true,
  get: function get() {
    return _Prova.provaProps;
  }
});