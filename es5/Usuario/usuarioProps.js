'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var usuarioProps = exports.usuarioProps = {
  id: {
    type: 'string',
    auto: {
      create: true
    }
  },
  dataCadastro: {
    type: 'date',
    auto: {
      create: true
    }
  },
  dataUltimaAlteracao: {
    type: 'date',
    auto: {
      create: true,
      update: true
    }
  },
  criadoPor: {
    type: 'string',
    auto: {
      create: true
    }
  },
  nome: {
    type: 'string',
    required: true
  },
  password: {
    type: 'string',
    required: true,
    sensivel: true,
    validar: function validar(value) {
      var validacao = {
        sucesso: true,
        erros: []
      };
      if (value.length < 8) {
        validacao.sucesso = false;
        validacao.erros.push({ message: 'A senha deve ter pelo menos oito caracteres.', code: 'senha_pequena' });
      }
      return validacao;
    }
  },
  username: {
    type: 'string'
  },
  instituicao: {
    type: 'string'
  },
  email: {
    type: 'string',
    required: true,
    validar: function validar(value) {
      var validacao = {
        sucesso: true,
        erros: []
      };
      var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!regexEmail.test(value)) {
        validacao.sucesso = false;
        validacao.erros.push({
          message: 'Email inválido.',
          code: 'email_invalido'
        });
      }
      return validacao;
    }
  },
  permissoes: {
    type: 'array'
  }
};