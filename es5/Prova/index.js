'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Prova = require('./Prova');

Object.defineProperty(exports, 'Prova', {
  enumerable: true,
  get: function get() {
    return _Prova.Prova;
  }
});

var _provaProps = require('./provaProps');

Object.defineProperty(exports, 'provaProps', {
  enumerable: true,
  get: function get() {
    return _provaProps.provaProps;
  }
});