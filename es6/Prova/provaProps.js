export const provasProps = {
  id: {
    type: 'string',
    auto: {
      create: true,
    },
  },
  dataCadastro: {
    type: 'date',
    auto: {
      create: true,
    },
  },
  dataUltimaAlteracao: {
    type: 'date',
    auto: {
      create: true,
      update: true,
    },
  },
  criadoPor: {
    type: 'string',
    auto: {
      create: true,
    },
  },
  usuarioId: {
    type: 'string',
    auto: {
      create: true,
    },
  },
  descricao: {
    type: 'string',
    required: true,
  },
  titulo: {
    type: 'string',
    required: true,
  },
  tema: {
    type: 'string',
  },
  dataAplicacao: {
    type: 'date',
  },
  instituicao: {
    type: 'string',
  },
  questoes: {
    type: 'array',
  },
}
