import { Entidade } from '../Entidade'
import { questaoProps } from './questaoProps'

export class Questao extends Entidade {
  constructor(data, operationType) {
    super(operationType)
    this.construirFront(questaoProps, data)
  }
}
