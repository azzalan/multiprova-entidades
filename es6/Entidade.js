export class Entidade {
  _data = {}

  _erros = null

  _props = {}

  _backProps = {}

  _operationType = null

  constructor(operationType = 'update') {
    this.operationType = operationType
  }

  construirFront(props, data) {
    this.props = props
    if (data) this.data = data
    else this._errors = { validandoBack: true }
  }

  construirBack(backProps, data) {
    this._errors = null
    this.backProps = backProps
    this.data = data
  }

  get operationType() {
    return this._operationType
  }

  set operationType(data) {
    this._operationType = data
  }

  get erros() {
    return this._erros
  }

  set erros(data) {
    throw new Error('Erros não podem ser setados manualmente.')
  }

  isValid() {
    return Boolean(!this.erros)
  }

  throwIfInvalid() {
    if (!this.isValid()) throw new Error('Objeto inválido não pode ser manipulado.')
  }

  get props() {
    return { ...this._props }
  }

  set props(data) {
    this._props = data
  }

  get backProps() {
    return new Error('Propriedade privada.')
  }

  set backProps(data) {
    this._backProps = data
  }

  get data() {
    return { ...this._data }
  }

  update(data) {
    this.operationType = 'update'
    let newData = { ...this.data, ...data }
    this.data = newData
    this.injetarAuto('update')
  }

  set data(newData) {
    this.injetarAuto(newData)
    const validacao = this.validar(newData)
    if (validacao.sucesso) {
      this._data = newData
      this._erros = null
    } else {
      this._data = null
      this._erros = validacao.erros
    }
  }

  injetarAuto(newData) {
    const { operationType } = this
    for (let propKey in this._backProps) {
      const autoProp = this._backProps[propKey].auto
      if (autoProp) {
        const autoOperationFunction = autoProp[operationType]
        if (autoOperationFunction) {
          newData[propKey] = autoOperationFunction(newData)
        }
      }
    }
  }

  validar(dataParam = null) {
    const validacao = {
      sucesso: true,
      erros: {},
    }
    let data = {}
    if (dataParam) {
      data = dataParam
    } else {
      data = this.data
    }
    for (let propKey in this._props) {
      const validacaoProp = this.validarProp(propKey, data[propKey])
      if (!validacaoProp.sucesso) {
        validacao.sucesso = false
        validacao.erros[propKey] = validacaoProp.erros
      }
    }
    for (let propKey in this._backProps) {
      const validacaoProp = this.validarPropBack(propKey, data[propKey])
      if (!validacaoProp.sucesso) {
        validacao.sucesso = false
        validacao.erros[propKey] = validacaoProp.erros
      }
    }
    return validacao
  }

  validarProp = (propKey, value) => {
    let { operationType } = this
    const propMetaData = this._props[propKey]
    const validacao = {
      sucesso: true,
      erros: [],
    }
    const hasValue = value !== undefined
    let hasAutoCreate = false
    let hasAutoUpdate = false
    if (propMetaData.auto) {
      if (propMetaData.auto.create) hasAutoCreate = true
      if (propMetaData.auto.update) hasAutoUpdate = true
    }
    if (propMetaData) {
      if (operationType === 'create' && propMetaData.required && !hasAutoCreate) {
        if (!hasValue) {
          validacao.sucesso = false
          validacao.erros.push({ message: 'Campo obrigatório.', code: 'campo_obrigatorio' })
        }
      }
      if (operationType === 'update' && propMetaData.required && !propMetaData.sensivel && !hasAutoUpdate) {
        if (!hasValue) {
          validacao.sucesso = false
          validacao.erros.push({ message: 'Campo obrigatório.', code: 'campo_obrigatorio' })
        }
      }
      if (propMetaData.type && hasValue) {
        if (propMetaData.type === 'array') {
          if (!Array.isArray(value)) {
            validacao.sucesso = false
            validacao.erros.push({ message: 'Esse campo precisa ter tipo ' + propMetaData.type, code: 'tipo_invalido' })
          }
        } else if (propMetaData.type === 'date') {
          // decidir como validar esse tipo
        } else if (propMetaData.type !== typeof value) {
          validacao.sucesso = false
          validacao.erros.push({ message: 'Esse campo precisa ter tipo ' + propMetaData.type, code: 'tipo_invalido' })
        }
      }
      if (propMetaData.validar && hasValue) {
        const metaValidacao = propMetaData.validar(value)
        if (!metaValidacao.sucesso) {
          validacao.sucesso = false
          validacao.erros = [...validacao.erros, ...metaValidacao.erros]
        }
      }
    }
    return validacao
  }

  validarPropBack(propKey, value) {
    const propMetaData = this._backProps[propKey]
    const validacao = {
      sucesso: true,
      erros: [],
    }
    const hasValue = value !== undefined
    if (propMetaData) {
      if (propMetaData.validar && hasValue) {
        const metaValidacao = propMetaData.validar(value)
        if (!metaValidacao.sucesso) {
          validacao.sucesso = false
          validacao.erros = [...validacao.erros, ...metaValidacao.erros]
        }
      }
    }
    return validacao
  }
}
