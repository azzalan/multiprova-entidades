'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Questao = require('./Questao');

Object.defineProperty(exports, 'Questao', {
  enumerable: true,
  get: function get() {
    return _Questao.Questao;
  }
});

var _questaoProps = require('./questaoProps');

Object.defineProperty(exports, 'questaoProps', {
  enumerable: true,
  get: function get() {
    return _questaoProps.questaoProps;
  }
});