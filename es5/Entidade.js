'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Entidade = exports.Entidade = function () {
  function Entidade() {
    var operationType = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'update';

    _classCallCheck(this, Entidade);

    _initialiseProps.call(this);

    this.operationType = operationType;
  }

  _createClass(Entidade, [{
    key: 'construirFront',
    value: function construirFront(props, data) {
      this.props = props;
      if (data) this.data = data;else this._errors = { validandoBack: true };
    }
  }, {
    key: 'construirBack',
    value: function construirBack(backProps, data) {
      this._errors = null;
      this.backProps = backProps;
      this.data = data;
    }
  }, {
    key: 'isValid',
    value: function isValid() {
      return Boolean(!this.erros);
    }
  }, {
    key: 'throwIfInvalid',
    value: function throwIfInvalid() {
      if (!this.isValid()) throw new Error('Objeto inválido não pode ser manipulado.');
    }
  }, {
    key: 'update',
    value: function update(data) {
      this.operationType = 'update';
      var newData = _extends({}, this.data, data);
      this.data = newData;
      this.injetarAuto('update');
    }
  }, {
    key: 'injetarAuto',
    value: function injetarAuto(newData) {
      var operationType = this.operationType;

      for (var propKey in this._backProps) {
        var autoProp = this._backProps[propKey].auto;
        if (autoProp) {
          var autoOperationFunction = autoProp[operationType];
          if (autoOperationFunction) {
            newData[propKey] = autoOperationFunction(newData);
          }
        }
      }
    }
  }, {
    key: 'validar',
    value: function validar() {
      var dataParam = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      var validacao = {
        sucesso: true,
        erros: {}
      };
      var data = {};
      if (dataParam) {
        data = dataParam;
      } else {
        data = this.data;
      }
      for (var propKey in this._props) {
        var validacaoProp = this.validarProp(propKey, data[propKey]);
        if (!validacaoProp.sucesso) {
          validacao.sucesso = false;
          validacao.erros[propKey] = validacaoProp.erros;
        }
      }
      for (var _propKey in this._backProps) {
        var _validacaoProp = this.validarPropBack(_propKey, data[_propKey]);
        if (!_validacaoProp.sucesso) {
          validacao.sucesso = false;
          validacao.erros[_propKey] = _validacaoProp.erros;
        }
      }
      return validacao;
    }
  }, {
    key: 'validarPropBack',
    value: function validarPropBack(propKey, value) {
      var propMetaData = this._backProps[propKey];
      var validacao = {
        sucesso: true,
        erros: []
      };
      var hasValue = value !== undefined;
      if (propMetaData) {
        if (propMetaData.validar && hasValue) {
          var metaValidacao = propMetaData.validar(value);
          if (!metaValidacao.sucesso) {
            validacao.sucesso = false;
            validacao.erros = [].concat(_toConsumableArray(validacao.erros), _toConsumableArray(metaValidacao.erros));
          }
        }
      }
      return validacao;
    }
  }, {
    key: 'operationType',
    get: function get() {
      return this._operationType;
    },
    set: function set(data) {
      this._operationType = data;
    }
  }, {
    key: 'erros',
    get: function get() {
      return this._erros;
    },
    set: function set(data) {
      throw new Error('Erros não podem ser setados manualmente.');
    }
  }, {
    key: 'props',
    get: function get() {
      return _extends({}, this._props);
    },
    set: function set(data) {
      this._props = data;
    }
  }, {
    key: 'backProps',
    get: function get() {
      return new Error('Propriedade privada.');
    },
    set: function set(data) {
      this._backProps = data;
    }
  }, {
    key: 'data',
    get: function get() {
      return _extends({}, this._data);
    },
    set: function set(newData) {
      this.injetarAuto(newData);
      var validacao = this.validar(newData);
      if (validacao.sucesso) {
        this._data = newData;
        this._erros = null;
      } else {
        this._data = null;
        this._erros = validacao.erros;
      }
    }
  }]);

  return Entidade;
}();

var _initialiseProps = function _initialiseProps() {
  var _this = this;

  this._data = {};
  this._erros = null;
  this._props = {};
  this._backProps = {};
  this._operationType = null;

  this.validarProp = function (propKey, value) {
    var operationType = _this.operationType;

    var propMetaData = _this._props[propKey];
    var validacao = {
      sucesso: true,
      erros: []
    };
    var hasValue = value !== undefined;
    var hasAutoCreate = false;
    var hasAutoUpdate = false;
    if (propMetaData.auto) {
      if (propMetaData.auto.create) hasAutoCreate = true;
      if (propMetaData.auto.update) hasAutoUpdate = true;
    }
    if (propMetaData) {
      if (operationType === 'create' && propMetaData.required && !hasAutoCreate) {
        if (!hasValue) {
          validacao.sucesso = false;
          validacao.erros.push({ message: 'Campo obrigatório.', code: 'campo_obrigatorio' });
        }
      }
      if (operationType === 'update' && propMetaData.required && !propMetaData.sensivel && !hasAutoUpdate) {
        if (!hasValue) {
          validacao.sucesso = false;
          validacao.erros.push({ message: 'Campo obrigatório.', code: 'campo_obrigatorio' });
        }
      }
      if (propMetaData.type && hasValue) {
        if (propMetaData.type === 'array') {
          if (!Array.isArray(value)) {
            validacao.sucesso = false;
            validacao.erros.push({ message: 'Esse campo precisa ter tipo ' + propMetaData.type, code: 'tipo_invalido' });
          }
        } else if (propMetaData.type === 'date') {
          // decidir como validar esse tipo
        } else if (propMetaData.type !== (typeof value === 'undefined' ? 'undefined' : _typeof(value))) {
          validacao.sucesso = false;
          validacao.erros.push({ message: 'Esse campo precisa ter tipo ' + propMetaData.type, code: 'tipo_invalido' });
        }
      }
      if (propMetaData.validar && hasValue) {
        var metaValidacao = propMetaData.validar(value);
        if (!metaValidacao.sucesso) {
          validacao.sucesso = false;
          validacao.erros = [].concat(_toConsumableArray(validacao.erros), _toConsumableArray(metaValidacao.erros));
        }
      }
    }
    return validacao;
  };
};